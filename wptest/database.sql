/*
SQLyog Ultimate v12.4.1 (64 bit)
MySQL - 5.6.37 : Database - wp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`wp` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `wp`;

/*Table structure for table `testcommentmeta` */

DROP TABLE IF EXISTS `testcommentmeta`;

CREATE TABLE `testcommentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `testcommentmeta` */

/*Table structure for table `testcomments` */

DROP TABLE IF EXISTS `testcomments`;

CREATE TABLE `testcomments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `testcomments` */

insert  into `testcomments`(`comment_ID`,`comment_post_ID`,`comment_author`,`comment_author_email`,`comment_author_url`,`comment_author_IP`,`comment_date`,`comment_date_gmt`,`comment_content`,`comment_karma`,`comment_approved`,`comment_agent`,`comment_type`,`comment_parent`,`user_id`) values 
(1,1,'A WordPress Commenter','wapuu@wordpress.example','https://wordpress.org/','','2019-02-01 11:16:17','2019-02-01 11:16:17','Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.',0,'1','','',0,0);

/*Table structure for table `testlinks` */

DROP TABLE IF EXISTS `testlinks`;

CREATE TABLE `testlinks` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `testlinks` */

/*Table structure for table `testoptions` */

DROP TABLE IF EXISTS `testoptions`;

CREATE TABLE `testoptions` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `testoptions` */

insert  into `testoptions`(`option_id`,`option_name`,`option_value`,`autoload`) values 
(1,'siteurl','http://localhost/wptest','yes'),
(2,'home','http://localhost/wptest','yes'),
(3,'blogname','Test Wordpress','yes'),
(4,'blogdescription','Test Wordpress Blog','yes'),
(5,'users_can_register','0','yes'),
(6,'admin_email','bhongong@gmail.com','yes'),
(7,'start_of_week','1','yes'),
(8,'use_balanceTags','0','yes'),
(9,'use_smilies','1','yes'),
(10,'require_name_email','1','yes'),
(11,'comments_notify','1','yes'),
(12,'posts_per_rss','10','yes'),
(13,'rss_use_excerpt','0','yes'),
(14,'mailserver_url','mail.example.com','yes'),
(15,'mailserver_login','login@example.com','yes'),
(16,'mailserver_pass','password','yes'),
(17,'mailserver_port','110','yes'),
(18,'default_category','1','yes'),
(19,'default_comment_status','open','yes'),
(20,'default_ping_status','open','yes'),
(21,'default_pingback_flag','1','yes'),
(22,'posts_per_page','10','yes'),
(23,'date_format','F j, Y','yes'),
(24,'time_format','g:i a','yes'),
(25,'links_updated_date_format','F j, Y g:i a','yes'),
(26,'comment_moderation','0','yes'),
(27,'moderation_notify','1','yes'),
(28,'permalink_structure','/%year%/%monthnum%/%day%/%postname%/','yes'),
(29,'rewrite_rules','a:90:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=9&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}','yes'),
(30,'hack_file','0','yes'),
(31,'blog_charset','UTF-8','yes'),
(32,'moderation_keys','','no'),
(33,'active_plugins','a:1:{i:0;s:36:\"contact-form-7/wp-contact-form-7.php\";}','yes'),
(34,'category_base','','yes'),
(35,'ping_sites','http://rpc.pingomatic.com/','yes'),
(36,'comment_max_links','2','yes'),
(37,'gmt_offset','','yes'),
(38,'default_email_category','1','yes'),
(39,'recently_edited','a:3:{i:0;s:64:\"E:\\Ampps\\www\\wptest/wp-content/themes/zerif-lite-child/style.css\";i:2;s:68:\"E:\\Ampps\\www\\wptest/wp-content/themes/zerif-lite-child/functions.php\";i:3;s:0:\"\";}','no'),
(40,'template','zerif-lite','yes'),
(41,'stylesheet','zerif-lite-child','yes'),
(42,'comment_whitelist','1','yes'),
(43,'blacklist_keys','','no'),
(44,'comment_registration','0','yes'),
(45,'html_type','text/html','yes'),
(46,'use_trackback','0','yes'),
(47,'default_role','subscriber','yes'),
(48,'db_version','43764','yes'),
(49,'uploads_use_yearmonth_folders','1','yes'),
(50,'upload_path','','yes'),
(51,'blog_public','1','yes'),
(52,'default_link_category','2','yes'),
(53,'show_on_front','page','yes'),
(54,'tag_base','','yes'),
(55,'show_avatars','1','yes'),
(56,'avatar_rating','G','yes'),
(57,'upload_url_path','','yes'),
(58,'thumbnail_size_w','150','yes'),
(59,'thumbnail_size_h','150','yes'),
(60,'thumbnail_crop','1','yes'),
(61,'medium_size_w','300','yes'),
(62,'medium_size_h','300','yes'),
(63,'avatar_default','mystery','yes'),
(64,'large_size_w','1024','yes'),
(65,'large_size_h','1024','yes'),
(66,'image_default_link_type','none','yes'),
(67,'image_default_size','','yes'),
(68,'image_default_align','','yes'),
(69,'close_comments_for_old_posts','0','yes'),
(70,'close_comments_days_old','14','yes'),
(71,'thread_comments','1','yes'),
(72,'thread_comments_depth','5','yes'),
(73,'page_comments','0','yes'),
(74,'comments_per_page','50','yes'),
(75,'default_comments_page','newest','yes'),
(76,'comment_order','asc','yes'),
(77,'sticky_posts','a:0:{}','yes'),
(78,'widget_categories','a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes'),
(79,'widget_text','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes'),
(80,'widget_rss','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes'),
(81,'uninstall_plugins','a:0:{}','no'),
(82,'timezone_string','Asia/Manila','yes'),
(83,'page_for_posts','10','yes'),
(84,'page_on_front','9','yes'),
(85,'default_post_format','0','yes'),
(86,'link_manager_enabled','0','yes'),
(87,'finished_splitting_shared_terms','1','yes'),
(88,'site_icon','0','yes'),
(89,'medium_large_size_w','768','yes'),
(90,'medium_large_size_h','0','yes'),
(91,'wp_page_for_privacy_policy','3','yes'),
(92,'show_comments_cookies_opt_in','0','yes'),
(93,'initial_db_version','43764','yes'),
(94,'testuser_roles','a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}','yes'),
(95,'fresh_site','0','yes'),
(96,'widget_search','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes'),
(97,'widget_recent-posts','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes'),
(98,'widget_recent-comments','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes'),
(99,'widget_archives','a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes'),
(100,'widget_meta','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes'),
(101,'sidebars_widgets','a:10:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:15:\"sidebar-aboutus\";a:0:{}s:20:\"zerif-sidebar-footer\";a:0:{}s:22:\"zerif-sidebar-footer-2\";a:0:{}s:22:\"zerif-sidebar-footer-3\";a:0:{}s:16:\"sidebar-ourfocus\";a:0:{}s:20:\"sidebar-testimonials\";a:0:{}s:15:\"sidebar-ourteam\";a:0:{}s:13:\"array_version\";i:3;}','yes'),
(102,'widget_pages','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
(103,'widget_calendar','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
(104,'widget_media_audio','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
(105,'widget_media_image','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
(106,'widget_media_gallery','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
(107,'widget_media_video','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
(108,'widget_tag_cloud','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
(109,'widget_nav_menu','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
(110,'widget_custom_html','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
(111,'cron','a:5:{i:1549707377;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1549710977;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1549711026;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1549712756;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}','yes'),
(113,'_site_transient_update_core','O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.0.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.0.3-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.0.3\";s:7:\"version\";s:5:\"5.0.3\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1549697781;s:15:\"version_checked\";s:5:\"5.0.3\";s:12:\"translations\";a:0:{}}','no'),
(114,'theme_mods_twentynineteen','a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1549019881;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}','yes'),
(124,'can_compress_scripts','1','no'),
(139,'recently_activated','a:0:{}','yes'),
(142,'_site_transient_update_themes','O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1549697784;s:7:\"checked\";a:5:{s:14:\"twentynineteen\";s:3:\"1.2\";s:15:\"twentyseventeen\";s:3:\"2.0\";s:13:\"twentysixteen\";s:3:\"1.8\";s:16:\"zerif-lite-child\";s:5:\"1.0.0\";s:10:\"zerif-lite\";s:8:\"1.8.5.52\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}','no'),
(143,'current_theme','Zelle Lite Child','yes'),
(144,'theme_mods_zerif-lite','a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1549020538;s:4:\"data\";a:9:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:15:\"sidebar-aboutus\";a:0:{}s:20:\"zerif-sidebar-footer\";a:0:{}s:22:\"zerif-sidebar-footer-2\";a:0:{}s:22:\"zerif-sidebar-footer-3\";a:0:{}s:16:\"sidebar-ourfocus\";a:0:{}s:20:\"sidebar-testimonials\";a:0:{}s:15:\"sidebar-ourteam\";a:0:{}}}}','yes'),
(145,'theme_switched','','yes'),
(146,'zerif_lite_install','1549019881','yes'),
(147,'zerif_time_activated','1549020539','yes'),
(158,'theme_mods_zerif-lite-child','a:3:{s:18:\"custom_css_post_id\";i:-1;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:11:\"custom_logo\";i:16;}','yes'),
(160,'theme_switch_menu_locations','a:0:{}','yes'),
(161,'theme_switched_via_customizer','','yes'),
(162,'nav_menu_options','a:1:{s:8:\"auto_add\";a:0:{}}','yes'),
(163,'customize_stashed_theme_mods','a:1:{s:10:\"zerif-lite\";a:1:{s:27:\"nav_menu_locations[primary]\";a:5:{s:15:\"starter_content\";b:1;s:5:\"value\";i:-1;s:4:\"type\";s:9:\"theme_mod\";s:7:\"user_id\";i:1;s:17:\"date_modified_gmt\";s:19:\"2019-02-01 11:18:54\";}}}','no'),
(177,'WPLANG','','yes'),
(178,'new_admin_email','bhongong@gmail.com','yes'),
(186,'_site_transient_update_plugins','O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1549697784;s:7:\"checked\";a:3:{s:19:\"akismet/akismet.php\";s:5:\"4.1.1\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.1\";s:9:\"hello.php\";s:5:\"1.7.1\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:3:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.1\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}}}','no'),
(187,'wpcf7','a:2:{s:7:\"version\";s:5:\"5.1.1\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";d:1549049981;s:7:\"version\";s:5:\"5.1.1\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}','yes'),
(199,'_site_transient_timeout_browser_591a43aafb2c3e203f6b925e7cd15e92','1550199689','no'),
(200,'_site_transient_browser_591a43aafb2c3e203f6b925e7cd15e92','a:10:{s:4:\"name\";s:7:\"Firefox\";s:7:\"version\";s:4:\"65.0\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:24:\"https://www.firefox.com/\";s:7:\"img_src\";s:44:\"http://s.w.org/images/browsers/firefox.png?1\";s:11:\"img_src_ssl\";s:45:\"https://s.w.org/images/browsers/firefox.png?1\";s:15:\"current_version\";s:2:\"56\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}','no'),
(201,'themeisle_sdk_notifications','a:2:{s:17:\"last_notification\";a:2:{s:2:\"id\";s:22:\"zerif_lite_review_flag\";s:10:\"display_at\";i:1549594890;}s:24:\"last_notification_active\";i:1549594900;}','yes'),
(206,'_transient_timeout_themeisle_sdk_feed_items','1549767692','no'),
(207,'_transient_themeisle_sdk_feed_items','a:5:{i:0;a:3:{s:5:\"title\";s:68:\"How to Create an Event Registration Website With WordPress (4 Steps)\";s:4:\"date\";i:1549533679;s:4:\"link\";s:54:\"https://themeisle.com/blog/event-registration-website/\";}i:1;a:3:{s:5:\"title\";s:51:\"WordPress Comments: 3 Tips for Managing Them Better\";s:4:\"date\";i:1549444866;s:4:\"link\";s:46:\"https://themeisle.com/blog/wordpress-comments/\";}i:2;a:3:{s:5:\"title\";s:58:\"How to Set Up G Suite for Your Website: Step-by-Step Guide\";s:4:\"date\";i:1549360821;s:4:\"link\";s:49:\"https://themeisle.com/blog/how-to-set-up-g-suite/\";}i:3;a:3:{s:5:\"title\";s:70:\"What Is WordCamp? Everything You Need to Know, Plus Tips for Attending\";s:4:\"date\";i:1549262495;s:4:\"link\";s:44:\"https://themeisle.com/blog/what-is-wordcamp/\";}i:4;a:3:{s:5:\"title\";s:57:\"How to Create Custom Headers &amp; Footers With Elementor\";s:4:\"date\";i:1548935963;s:4:\"link\";s:72:\"https://themeisle.com/blog/create-custom-headers-footers-with-elementor/\";}}','no'),
(212,'zerif_lite_review_flag','no','yes'),
(228,'_site_transient_timeout_theme_roots','1549699583','no'),
(229,'_site_transient_theme_roots','a:5:{s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";s:16:\"zerif-lite-child\";s:7:\"/themes\";s:10:\"zerif-lite\";s:7:\"/themes\";}','no'),
(233,'_transient_timeout_oembed_bcf67f637d59e43a20adbbbbca0535ae','1549784627','no'),
(234,'_transient_oembed_bcf67f637d59e43a20adbbbbca0535ae','O:8:\"stdClass\":13:{s:11:\"author_name\";s:14:\"Calixto Ong II\";s:6:\"height\";i:450;s:4:\"html\";s:217:\"<iframe width=\"600\" height=\"450\" src=\"https://www.youtube.com/embed/uErbyJyxvnc?feature=oembed\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\";s:4:\"type\";s:5:\"video\";s:15:\"thumbnail_width\";i:480;s:13:\"thumbnail_url\";s:48:\"https://i.ytimg.com/vi/uErbyJyxvnc/hqdefault.jpg\";s:12:\"provider_url\";s:24:\"https://www.youtube.com/\";s:16:\"thumbnail_height\";i:360;s:7:\"version\";s:3:\"1.0\";s:10:\"author_url\";s:37:\"https://www.youtube.com/user/bhongong\";s:5:\"title\";s:17:\"I was guest on TV\";s:5:\"width\";i:600;s:13:\"provider_name\";s:7:\"YouTube\";}','no'),
(242,'_transient_timeout_ti_customizer_notify_plugin_information_transient_themeisle-companion','1549704099','no'),
(243,'_transient_ti_customizer_notify_plugin_information_transient_themeisle-companion','O:8:\"stdClass\":15:{s:4:\"name\";s:22:\"Orbit Fox by ThemeIsle\";s:4:\"slug\";s:19:\"themeisle-companion\";s:7:\"version\";s:5:\"2.7.5\";s:6:\"author\";s:45:\"<a href=\"https://orbitfox.com/\">Themeisle</a>\";s:14:\"author_profile\";s:40:\"https://profiles.wordpress.org/themeisle\";s:12:\"requires_php\";s:3:\"5.4\";s:7:\"ratings\";a:5:{i:5;i:95;i:4;i:9;i:3;i:3;i:2;i:1;i:1;i:4;}s:11:\"num_ratings\";i:112;s:15:\"support_threads\";i:21;s:24:\"support_threads_resolved\";i:8;s:17:\"short_description\";s:159:\"This swiss-knife plugin comes with a quality template library, menu/sharing icons, Gutenberg blocks and newly added Elementor/BeaverBuilder page build &hellip;\";s:13:\"download_link\";s:62:\"https://downloads.wordpress.org/plugin/themeisle-companion.zip\";s:11:\"screenshots\";a:5:{i:1;a:2:{s:3:\"src\";s:72:\"https://ps.w.org/themeisle-companion/assets/screenshot-1.png?rev=1807829\";s:7:\"caption\";s:27:\"The OrbitFox Dashboard Page\";}i:2;a:2:{s:3:\"src\";s:72:\"https://ps.w.org/themeisle-companion/assets/screenshot-2.png?rev=1807829\";s:7:\"caption\";s:27:\"The Template Directory Page\";}i:3;a:2:{s:3:\"src\";s:72:\"https://ps.w.org/themeisle-companion/assets/screenshot-3.png?rev=1807829\";s:7:\"caption\";s:17:\"Menu Icons Module\";}i:4;a:2:{s:3:\"src\";s:72:\"https://ps.w.org/themeisle-companion/assets/screenshot-4.png?rev=1807829\";s:7:\"caption\";s:31:\"Page Builder Widgets and Addons\";}i:5;a:2:{s:3:\"src\";s:72:\"https://ps.w.org/themeisle-companion/assets/screenshot-5.png?rev=1807829\";s:7:\"caption\";s:21:\"Social Sharing Module\";}}s:8:\"versions\";a:61:{s:5:\"1.0.0\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.1.0.0.zip\";s:5:\"1.0.1\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.1.0.1.zip\";s:5:\"1.0.2\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.1.0.2.zip\";s:5:\"1.0.3\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.1.0.3.zip\";s:5:\"1.0.4\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.1.0.4.zip\";s:5:\"1.0.5\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.1.0.5.zip\";s:5:\"2.0.0\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.0.0.zip\";s:5:\"2.0.1\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.0.1.zip\";s:6:\"2.0.10\";s:69:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.0.10.zip\";s:6:\"2.0.11\";s:69:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.0.11.zip\";s:5:\"2.0.2\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.0.2.zip\";s:5:\"2.0.3\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.0.3.zip\";s:5:\"2.0.4\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.0.4.zip\";s:5:\"2.0.5\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.0.5.zip\";s:5:\"2.0.6\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.0.6.zip\";s:5:\"2.0.7\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.0.7.zip\";s:5:\"2.0.8\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.0.8.zip\";s:5:\"2.0.9\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.0.9.zip\";s:5:\"2.1.0\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.1.0.zip\";s:5:\"2.1.1\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.1.1.zip\";s:5:\"2.2.0\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.2.0.zip\";s:5:\"2.2.1\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.2.1.zip\";s:5:\"2.2.2\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.2.2.zip\";s:5:\"2.2.3\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.2.3.zip\";s:5:\"2.2.4\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.2.4.zip\";s:5:\"2.2.5\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.2.5.zip\";s:5:\"2.2.6\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.2.6.zip\";s:5:\"2.2.7\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.2.7.zip\";s:5:\"2.3.0\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.3.0.zip\";s:5:\"2.3.1\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.3.1.zip\";s:5:\"2.3.2\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.3.2.zip\";s:5:\"2.3.3\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.3.3.zip\";s:5:\"2.3.4\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.3.4.zip\";s:5:\"2.4.0\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.4.0.zip\";s:5:\"2.4.1\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.4.1.zip\";s:5:\"2.4.2\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.4.2.zip\";s:5:\"2.4.3\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.4.3.zip\";s:5:\"2.4.4\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.4.4.zip\";s:5:\"2.4.5\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.4.5.zip\";s:5:\"2.4.6\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.4.6.zip\";s:5:\"2.4.7\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.4.7.zip\";s:5:\"2.5.0\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.5.0.zip\";s:5:\"2.5.1\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.5.1.zip\";s:5:\"2.5.2\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.5.2.zip\";s:5:\"2.5.3\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.5.3.zip\";s:5:\"2.5.4\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.5.4.zip\";s:5:\"2.5.5\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.5.5.zip\";s:5:\"2.5.6\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.5.6.zip\";s:5:\"2.5.7\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.5.7.zip\";s:5:\"2.5.8\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.5.8.zip\";s:5:\"2.6.0\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.6.0.zip\";s:5:\"2.6.1\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.6.1.zip\";s:5:\"2.6.2\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.6.2.zip\";s:5:\"2.6.3\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.6.3.zip\";s:5:\"2.6.4\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.6.4.zip\";s:5:\"2.7.0\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.7.0.zip\";s:5:\"2.7.1\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.7.1.zip\";s:5:\"2.7.2\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.7.2.zip\";s:5:\"2.7.3\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.7.3.zip\";s:5:\"2.7.4\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.7.4.zip\";s:5:\"2.7.5\";s:68:\"https://downloads.wordpress.org/plugin/themeisle-companion.2.7.5.zip\";}s:12:\"contributors\";a:0:{}}','no'),
(247,'_transient_is_multi_author','0','yes');

/*Table structure for table `testpostmeta` */

DROP TABLE IF EXISTS `testpostmeta`;

CREATE TABLE `testpostmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `testpostmeta` */

insert  into `testpostmeta`(`meta_id`,`post_id`,`meta_key`,`meta_value`) values 
(1,2,'_wp_page_template','default'),
(2,3,'_wp_page_template','default'),
(10,9,'_customize_changeset_uuid','bc5b7e4c-a676-4674-8d09-f4078583d584'),
(12,10,'_customize_changeset_uuid','bc5b7e4c-a676-4674-8d09-f4078583d584'),
(13,11,'_edit_lock','1549020506:1'),
(14,14,'_menu_item_type','post_type'),
(15,14,'_menu_item_menu_item_parent','0'),
(16,14,'_menu_item_object_id','9'),
(17,14,'_menu_item_object','page'),
(18,14,'_menu_item_target',''),
(19,14,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
(20,14,'_menu_item_xfn',''),
(21,14,'_menu_item_url',''),
(22,15,'_menu_item_type','post_type'),
(23,15,'_menu_item_menu_item_parent','0'),
(24,15,'_menu_item_object_id','10'),
(25,15,'_menu_item_object','page'),
(26,15,'_menu_item_target',''),
(27,15,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
(28,15,'_menu_item_xfn',''),
(29,15,'_menu_item_url',''),
(30,11,'_wp_trash_meta_status','publish'),
(31,11,'_wp_trash_meta_time','1549020539'),
(33,16,'_wp_attached_file','2019/02/logo.png'),
(34,16,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:132;s:6:\"height\";i:34;s:4:\"file\";s:16:\"2019/02/logo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(35,17,'_edit_lock','1549021077:1'),
(36,17,'_wp_trash_meta_status','publish'),
(37,17,'_wp_trash_meta_time','1549021086'),
(40,19,'_form','<label> Your Name (required)\n    [text* Name] </label>\n\n<label> Your Email (required)\n    [email* Email] </label>\n\n<label> Your Message\n    [textarea Content] </label>\n\n[submit \"Send\"]'),
(41,19,'_mail','a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:31:\"Test Wordpress \"[your-subject]\"\";s:6:\"sender\";s:35:\"Test Wordpress <bhongong@gmail.com>\";s:9:\"recipient\";s:18:\"bhongong@gmail.com\";s:4:\"body\";s:176:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Test Wordpress (http://localhost/wptest)\";s:18:\"additional_headers\";s:28:\"Reply-To: bhongong@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(42,19,'_mail_2','a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:31:\"Test Wordpress \"[your-subject]\"\";s:6:\"sender\";s:35:\"Test Wordpress <bhongong@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:118:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Test Wordpress (http://localhost/wptest)\";s:18:\"additional_headers\";s:28:\"Reply-To: bhongong@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(43,19,'_messages','a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(44,19,'_additional_settings',''),
(45,19,'_locale','en_US'),
(48,9,'_edit_lock','1549021452:1'),
(49,21,'_edit_lock','1549704803:1'),
(50,2,'_edit_lock','1549021718:1'),
(51,25,'_menu_item_type','post_type'),
(52,25,'_menu_item_menu_item_parent','0'),
(53,25,'_menu_item_object_id','21'),
(54,25,'_menu_item_object','page'),
(55,25,'_menu_item_target',''),
(56,25,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
(57,25,'_menu_item_xfn',''),
(58,25,'_menu_item_url',''),
(59,21,'_wp_page_template','custom-fullwidth-no-title.php'),
(60,21,'_oembed_ceb92e860cc1a177bd92db83836092f8','<iframe width=\"640\" height=\"480\" src=\"https://www.youtube.com/embed/uErbyJyxvnc?feature=oembed\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(61,21,'_oembed_time_ceb92e860cc1a177bd92db83836092f8','1549701881');

/*Table structure for table `testposts` */

DROP TABLE IF EXISTS `testposts`;

CREATE TABLE `testposts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `testposts` */

insert  into `testposts`(`ID`,`post_author`,`post_date`,`post_date_gmt`,`post_content`,`post_title`,`post_excerpt`,`post_status`,`comment_status`,`ping_status`,`post_password`,`post_name`,`to_ping`,`pinged`,`post_modified`,`post_modified_gmt`,`post_content_filtered`,`post_parent`,`guid`,`menu_order`,`post_type`,`post_mime_type`,`comment_count`) values 
(1,1,'2019-02-01 11:16:17','2019-02-01 11:16:17','<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->','Hello world!','','publish','open','open','','hello-world','','','2019-02-01 11:16:17','2019-02-01 11:16:17','',0,'http://127.0.0.1/wptest/?p=1',0,'post','',1),
(2,1,'2019-02-01 11:16:17','2019-02-01 11:16:17','<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://127.0.0.1/wptest/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->','Sample Page','','publish','closed','open','','sample-page','','','2019-02-01 11:16:17','2019-02-01 11:16:17','',0,'http://127.0.0.1/wptest/?page_id=2',0,'page','',0),
(3,1,'2019-02-01 11:16:17','2019-02-01 11:16:17','<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://127.0.0.1/wptest.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->','Privacy Policy','','draft','closed','open','','privacy-policy','','','2019-02-01 11:16:17','2019-02-01 11:16:17','',0,'http://127.0.0.1/wptest/?page_id=3',0,'page','',0),
(9,1,'2019-02-01 11:28:58','2019-02-01 11:28:58','Welcome to your site! This is your homepage, which is what most visitors will see when they come to your site for the first time.','Home','','publish','closed','closed','','home','','','2019-02-01 11:28:58','2019-02-01 11:28:58','',0,'http://127.0.0.1/wptest/?page_id=9',0,'page','',0),
(10,1,'2019-02-01 11:28:58','2019-02-01 11:28:58','','Blog','','publish','closed','closed','','blog','','','2019-02-01 11:28:58','2019-02-01 11:28:58','',0,'http://127.0.0.1/wptest/?page_id=10',0,'page','',0),
(11,1,'2019-02-01 11:28:58','2019-02-01 11:28:58','{\n    \"nav_menus_created_posts\": {\n        \"starter_content\": true,\n        \"value\": [\n            9,\n            10\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-02-01 11:18:54\"\n    },\n    \"nav_menu[-1]\": {\n        \"value\": {\n            \"name\": \"Primary Menu\",\n            \"description\": \"\",\n            \"parent\": 0,\n            \"auto_add\": false\n        },\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-02-01 11:28:58\"\n    },\n    \"nav_menu_item[-1]\": {\n        \"value\": {\n            \"object_id\": 9,\n            \"object\": \"page\",\n            \"menu_item_parent\": 0,\n            \"position\": 0,\n            \"type\": \"post_type\",\n            \"title\": \"Home\",\n            \"url\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"Home\",\n            \"nav_menu_term_id\": -1,\n            \"_invalid\": false,\n            \"type_label\": \"Page\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-02-01 11:28:58\"\n    },\n    \"nav_menu_item[-2]\": {\n        \"value\": {\n            \"object_id\": 10,\n            \"object\": \"page\",\n            \"menu_item_parent\": 0,\n            \"position\": 1,\n            \"type\": \"post_type\",\n            \"title\": \"Blog\",\n            \"url\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"Blog\",\n            \"nav_menu_term_id\": -1,\n            \"_invalid\": false,\n            \"type_label\": \"Page\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-02-01 11:28:58\"\n    },\n    \"zerif-lite::nav_menu_locations[primary]\": {\n        \"starter_content\": true,\n        \"value\": -1,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-02-01 11:18:54\"\n    },\n    \"show_on_front\": {\n        \"starter_content\": true,\n        \"value\": \"page\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-02-01 11:18:54\"\n    },\n    \"page_on_front\": {\n        \"starter_content\": true,\n        \"value\": 9,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-02-01 11:18:54\"\n    },\n    \"page_for_posts\": {\n        \"starter_content\": true,\n        \"value\": 10,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-02-01 11:18:54\"\n    },\n    \"zerif-lite-child::nav_menu_locations[primary]\": {\n        \"starter_content\": true,\n        \"value\": -1,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-02-01 11:28:26\"\n    },\n    \"old_sidebars_widgets_data\": {\n        \"value\": {\n            \"wp_inactive_widgets\": [],\n            \"sidebar-1\": [\n                \"search-2\",\n                \"recent-posts-2\",\n                \"recent-comments-2\",\n                \"archives-2\",\n                \"categories-2\",\n                \"meta-2\"\n            ],\n            \"sidebar-aboutus\": [],\n            \"zerif-sidebar-footer\": [],\n            \"zerif-sidebar-footer-2\": [],\n            \"zerif-sidebar-footer-3\": [],\n            \"sidebar-ourfocus\": [],\n            \"sidebar-testimonials\": [],\n            \"sidebar-ourteam\": []\n        },\n        \"type\": \"global_variable\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-02-01 11:28:26\"\n    }\n}','','','trash','closed','closed','','bc5b7e4c-a676-4674-8d09-f4078583d584','','','2019-02-01 11:28:58','2019-02-01 11:28:58','',0,'http://127.0.0.1/wptest/?p=11',0,'customize_changeset','',0),
(12,1,'2019-02-01 11:28:58','2019-02-01 11:28:58','Welcome to your site! This is your homepage, which is what most visitors will see when they come to your site for the first time.','Home','','inherit','closed','closed','','9-revision-v1','','','2019-02-01 11:28:58','2019-02-01 11:28:58','',9,'http://127.0.0.1/wptest/2019/02/01/9-revision-v1/',0,'revision','',0),
(13,1,'2019-02-01 11:28:58','2019-02-01 11:28:58','','Blog','','inherit','closed','closed','','10-revision-v1','','','2019-02-01 11:28:58','2019-02-01 11:28:58','',10,'http://127.0.0.1/wptest/2019/02/01/10-revision-v1/',0,'revision','',0),
(14,1,'2019-02-01 11:28:58','2019-02-01 11:28:58',' ','','','publish','closed','closed','','14','','','2019-02-01 19:52:16','2019-02-01 11:52:16','',0,'http://127.0.0.1/wptest/2019/02/01/14/',1,'nav_menu_item','',0),
(15,1,'2019-02-01 11:28:58','2019-02-01 11:28:58',' ','','','publish','closed','closed','','15','','','2019-02-01 19:52:16','2019-02-01 11:52:16','',0,'http://127.0.0.1/wptest/2019/02/01/15/',3,'nav_menu_item','',0),
(16,1,'2019-02-01 19:37:46','2019-02-01 11:37:46','','logo','','inherit','open','closed','','logo','','','2019-02-01 19:37:46','2019-02-01 11:37:46','',0,'http://localhost/wptest/wp-content/uploads/2019/02/logo.png',0,'attachment','image/png',0),
(17,1,'2019-02-01 19:38:06','2019-02-01 11:38:06','{\n    \"zerif-lite-child::custom_logo\": {\n        \"value\": 16,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-02-01 11:37:57\"\n    }\n}','','','trash','closed','closed','','c29080a9-4b24-4c5a-8108-3d1dcc76dd1b','','','2019-02-01 19:38:06','2019-02-01 11:38:06','',0,'http://localhost/wptest/?p=17',0,'customize_changeset','',0),
(19,1,'2019-02-01 19:39:41','2019-02-01 11:39:41','<label> Your Name (required)\r\n    [text* Name] </label>\r\n\r\n<label> Your Email (required)\r\n    [email* Email] </label>\r\n\r\n<label> Your Message\r\n    [textarea Content] </label>\r\n\r\n[submit \"Send\"]\n1\nTest Wordpress \"[your-subject]\"\nTest Wordpress <bhongong@gmail.com>\nbhongong@gmail.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Test Wordpress (http://localhost/wptest)\nReply-To: bhongong@gmail.com\n\n\n\n\nTest Wordpress \"[your-subject]\"\nTest Wordpress <bhongong@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Test Wordpress (http://localhost/wptest)\nReply-To: bhongong@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.','Contact form 1','','publish','closed','closed','','contact-form-1','','','2019-02-01 19:41:47','2019-02-01 11:41:47','',0,'http://localhost/wptest/?post_type=wpcf7_contact_form&#038;p=19',0,'wpcf7_contact_form','',0),
(21,1,'2019-02-01 19:50:35','2019-02-01 11:50:35','<!-- wp:paragraph -->\n<p>Hi, I am Calixto Ong II or you can call me Bhong.  Software Development, coding and coaching is my passion and I have been in the software development industry since 1994.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I have numerous experience working on a project from concept, development, testing, deployment, training and maintenance. I have years of experience working on various business applications like cooperatives, membership, school system, financial, accounting, inventory, ecommerce, travel booking, MLM, call center and health services. &nbsp;I also take into consideration, following Secure Coding Practices to ensure protection against common web vulnerability and attacks (XSS, SQL Injection, Session Hijacking and others). I am also able to pickup an existing project, perform a code review, security audit, vulnerability check, documentation, add enhancements and functionality. &nbsp; </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I have working experience and knowledge in hardware and virtualized servers for web and database application. I can perform installation, configuration, maintenance and deployment of servers on shared hosting, VPS, dedicated servers in-house or remote datacenters as well as in cloud platforms like AWS and Digital Ocean. I have knowledge and awareness on Data Privacy Compliance, ISO Standards, ITIL, PCI DSS compliance, Disaster Recovery and Business Continuity concepts and practice. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You can download <a rel=\"noreferrer noopener\" aria-label=\"my resume (opens in a new tab)\" href=\"https://drive.google.com/file/d/17RNFwbQqZUrOYS9QtOr80j9-QG2ecavA/view?usp=sharing\" target=\"_blank\">my resume</a>, visit <a href=\"https://www.facebook.com/calixto.ong\">my facebook</a> or <a href=\"https://www.linkedin.com/in/bhongong/\" target=\"_blank\" rel=\"noreferrer noopener\" aria-label=\"linkedin profile (opens in a new tab)\">linkedin profile</a> for more details. <br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>Send me note:</h4>\n<!-- /wp:heading -->\n\n<!-- wp:shortcode -->\n[contact-form-7 id=\"19\" title=\"Contact form 1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','About Me','','publish','closed','closed','','about-me','','','2019-02-09 17:28:33','2019-02-09 09:28:33','',0,'http://localhost/wptest/?page_id=21',0,'page','',0),
(22,1,'2019-02-01 19:50:35','2019-02-01 11:50:35','<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[contact-form-7 id=\"19\" title=\"Contact form 1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','about me','','inherit','closed','closed','','21-revision-v1','','','2019-02-01 19:50:35','2019-02-01 11:50:35','',21,'http://localhost/wptest/2019/02/01/21-revision-v1/',0,'revision','',0),
(24,1,'2019-02-01 19:51:40','2019-02-01 11:51:40','<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[contact-form-7 id=\"19\" title=\"Contact form 1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','About Me','','inherit','closed','closed','','21-revision-v1','','','2019-02-01 19:51:40','2019-02-01 11:51:40','',21,'http://localhost/wptest/2019/02/01/21-revision-v1/',0,'revision','',0),
(25,1,'2019-02-01 19:52:16','2019-02-01 11:52:16',' ','','','publish','closed','closed','','25','','','2019-02-01 19:52:16','2019-02-01 11:52:16','',0,'http://localhost/wptest/?p=25',2,'nav_menu_item','',0),
(26,1,'2019-02-09 15:41:08','2019-02-09 07:41:08','<!-- wp:paragraph -->\n<p>This is a test content</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[contact-form-7 id=\"19\" title=\"Contact form 1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','About Me','','inherit','closed','closed','','21-revision-v1','','','2019-02-09 15:41:08','2019-02-09 07:41:08','',21,'http://localhost/wptest/2019/02/09/21-revision-v1/',0,'revision','',0),
(28,1,'2019-02-09 15:45:18','2019-02-09 07:45:18','<!-- wp:paragraph -->\n<p>This is a test content</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:core-embed/youtube {\"url\":\"https://www.youtube.com/watch?v=uErbyJyxvnc\",\"type\":\"video\",\"providerNameSlug\":\"youtube\",\"className\":\"wp-embed-aspect-4-3 wp-has-aspect-ratio\"} -->\n<figure class=\"wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-4-3 wp-has-aspect-ratio\"><div class=\"wp-block-embed__wrapper\">\nhttps://www.youtube.com/watch?v=uErbyJyxvnc\n</div></figure>\n<!-- /wp:core-embed/youtube -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[contact-form-7 id=\"19\" title=\"Contact form 1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','About Me','','inherit','closed','closed','','21-revision-v1','','','2019-02-09 15:45:18','2019-02-09 07:45:18','',21,'http://localhost/wptest/2019/02/09/21-revision-v1/',0,'revision','',0),
(29,1,'2019-02-09 16:08:48','2019-02-09 08:08:48','<!-- wp:paragraph -->\n<p>Hi, I am Calixto Ong II or you can call me Bhong.  Software Development, coding and coaching is my passion and I have been in the software development industry since 1994.  I have been working as employed, freelance in an office setting as well as an offshore and remote home based staff.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I have numerous experience working on a project from concept, development, testing, deployment, training and maintenance. I have years of experience working on various business applications like cooperatives, membership, school system, financial, accounting, inventory, ecommerce, travel booking, MLM, call center and health services.  I also take into consideration, following Secure Coding Practices to ensure protection against common web vulnerability and attacks (XSS, SQL Injection, Session Hijacking and others). I am also able to pickup an existing project, perform a code review, security audit, vulnerability check, documentation, add enhancements and functionality.   </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I have working experience and knowledge in hardware and virtualized servers for web and database application. I can perform installation, configuration, maintenance and deployment of servers on shared hosting, VPS, dedicated servers in-house or remote datacenters as well as in cloud platforms like AWS and Digital Ocean. I have knowledge and awareness on Data Privacy Compliance, ISO Standards, ITIL, PCI DSS compliance, Disaster Recovery and Business Continuity concepts and practice. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:core-embed/youtube {\"url\":\"https://www.youtube.com/watch?v=uErbyJyxvnc\",\"type\":\"video\",\"providerNameSlug\":\"youtube\",\"className\":\"wp-embed-aspect-4-3 wp-has-aspect-ratio\"} -->\n<figure class=\"wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-4-3 wp-has-aspect-ratio\"><div class=\"wp-block-embed__wrapper\">\nhttps://www.youtube.com/watch?v=uErbyJyxvnc\n</div></figure>\n<!-- /wp:core-embed/youtube -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[contact-form-7 id=\"19\" title=\"Contact form 1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','About Me','','inherit','closed','closed','','21-revision-v1','','','2019-02-09 16:08:48','2019-02-09 08:08:48','',21,'http://localhost/wptest/2019/02/09/21-revision-v1/',0,'revision','',0),
(30,1,'2019-02-09 16:18:37','2019-02-09 08:18:37','<!-- wp:paragraph -->\n<p>Hi, I am Calixto Ong II or you can call me Bhong.  Software Development, coding and coaching is my passion and I have been in the software development industry since 1994.  I have been working as employed, freelance in an office setting as well as an offshore and remote home based staff.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I have numerous experience working on a project from concept, development, testing, deployment, training and maintenance. I have years of experience working on various business applications like cooperatives, membership, school system, financial, accounting, inventory, ecommerce, travel booking, MLM, call center and health services.  I also take into consideration, following Secure Coding Practices to ensure protection against common web vulnerability and attacks (XSS, SQL Injection, Session Hijacking and others). I am also able to pickup an existing project, perform a code review, security audit, vulnerability check, documentation, add enhancements and functionality.   </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I have working experience and knowledge in hardware and virtualized servers for web and database application. I can perform installation, configuration, maintenance and deployment of servers on shared hosting, VPS, dedicated servers in-house or remote datacenters as well as in cloud platforms like AWS and Digital Ocean. I have knowledge and awareness on Data Privacy Compliance, ISO Standards, ITIL, PCI DSS compliance, Disaster Recovery and Business Continuity concepts and practice. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:core-embed/youtube {\"url\":\"https://www.youtube.com/watch?v=uErbyJyxvnc\",\"type\":\"video\",\"providerNameSlug\":\"youtube\",\"align\":\"center\",\"className\":\"wp-embed-aspect-4-3 wp-has-aspect-ratio\"} -->\n<figure class=\"wp-block-embed-youtube aligncenter wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-4-3 wp-has-aspect-ratio\"><div class=\"wp-block-embed__wrapper\">\nhttps://www.youtube.com/watch?v=uErbyJyxvnc\n</div></figure>\n<!-- /wp:core-embed/youtube -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[contact-form-7 id=\"19\" title=\"Contact form 1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','About Me','','inherit','closed','closed','','21-revision-v1','','','2019-02-09 16:18:37','2019-02-09 08:18:37','',21,'http://localhost/wptest/2019/02/09/21-revision-v1/',0,'revision','',0),
(32,1,'2019-02-09 16:19:47','2019-02-09 08:19:47','<!-- wp:paragraph -->\n<p>Hi, I am Calixto Ong II or you can call me Bhong.  Software Development, coding and coaching is my passion and I have been in the software development industry since 1994.  I have been working as employed, freelance in an office setting as well as an offshore and remote home based staff.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I have numerous experience working on a project from concept, development, testing, deployment, training and maintenance. I have years of experience working on various business applications like cooperatives, membership, school system, financial, accounting, inventory, ecommerce, travel booking, MLM, call center and health services. &nbsp;I also take into consideration, following Secure Coding Practices to ensure protection against common web vulnerability and attacks (XSS, SQL Injection, Session Hijacking and others). I am also able to pickup an existing project, perform a code review, security audit, vulnerability check, documentation, add enhancements and functionality. &nbsp; </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I have working experience and knowledge in hardware and virtualized servers for web and database application. I can perform installation, configuration, maintenance and deployment of servers on shared hosting, VPS, dedicated servers in-house or remote datacenters as well as in cloud platforms like AWS and Digital Ocean. I have knowledge and awareness on Data Privacy Compliance, ISO Standards, ITIL, PCI DSS compliance, Disaster Recovery and Business Continuity concepts and practice. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:core-embed/youtube {\"url\":\"https://www.youtube.com/watch?v=uErbyJyxvnc\",\"type\":\"video\",\"providerNameSlug\":\"youtube\",\"align\":\"center\",\"className\":\"wp-embed-aspect-4-3 wp-has-aspect-ratio\"} -->\n<figure class=\"wp-block-embed-youtube aligncenter wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-4-3 wp-has-aspect-ratio\"><div class=\"wp-block-embed__wrapper\">\nhttps://www.youtube.com/watch?v=uErbyJyxvnc\n</div></figure>\n<!-- /wp:core-embed/youtube -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Send me note:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[contact-form-7 id=\"19\" title=\"Contact form 1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','About Me','','inherit','closed','closed','','21-revision-v1','','','2019-02-09 16:19:47','2019-02-09 08:19:47','',21,'http://localhost/wptest/2019/02/09/21-revision-v1/',0,'revision','',0),
(34,1,'2019-02-09 16:24:19','2019-02-09 08:24:19','<!-- wp:paragraph -->\n<p>Hi, I am Calixto Ong II or you can call me Bhong.  Software Development, coding and coaching is my passion and I have been in the software development industry since 1994.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I have numerous experience working on a project from concept, development, testing, deployment, training and maintenance. I have years of experience working on various business applications like cooperatives, membership, school system, financial, accounting, inventory, ecommerce, travel booking, MLM, call center and health services. &nbsp;I also take into consideration, following Secure Coding Practices to ensure protection against common web vulnerability and attacks (XSS, SQL Injection, Session Hijacking and others). I am also able to pickup an existing project, perform a code review, security audit, vulnerability check, documentation, add enhancements and functionality. &nbsp; </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I have working experience and knowledge in hardware and virtualized servers for web and database application. I can perform installation, configuration, maintenance and deployment of servers on shared hosting, VPS, dedicated servers in-house or remote datacenters as well as in cloud platforms like AWS and Digital Ocean. I have knowledge and awareness on Data Privacy Compliance, ISO Standards, ITIL, PCI DSS compliance, Disaster Recovery and Business Continuity concepts and practice. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:core-embed/youtube {\"url\":\"https://www.youtube.com/watch?v=uErbyJyxvnc\",\"type\":\"video\",\"providerNameSlug\":\"youtube\",\"align\":\"center\",\"className\":\"wp-embed-aspect-4-3 wp-has-aspect-ratio\"} -->\n<figure class=\"wp-block-embed-youtube aligncenter wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-4-3 wp-has-aspect-ratio\"><div class=\"wp-block-embed__wrapper\">\nhttps://www.youtube.com/watch?v=uErbyJyxvnc\n</div></figure>\n<!-- /wp:core-embed/youtube -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Send me note:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[contact-form-7 id=\"19\" title=\"Contact form 1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','About Me','','inherit','closed','closed','','21-revision-v1','','','2019-02-09 16:24:19','2019-02-09 08:24:19','',21,'http://localhost/wptest/2019/02/09/21-revision-v1/',0,'revision','',0),
(40,1,'2019-02-09 16:51:31','2019-02-09 08:51:31','<!-- wp:paragraph -->\n<p>Hi, I am Calixto Ong II or you can call me Bhong.  Software Development, coding and coaching is my passion and I have been in the software development industry since 1994.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I have numerous experience working on a project from concept, development, testing, deployment, training and maintenance. I have years of experience working on various business applications like cooperatives, membership, school system, financial, accounting, inventory, ecommerce, travel booking, MLM, call center and health services. &nbsp;I also take into consideration, following Secure Coding Practices to ensure protection against common web vulnerability and attacks (XSS, SQL Injection, Session Hijacking and others). I am also able to pickup an existing project, perform a code review, security audit, vulnerability check, documentation, add enhancements and functionality. &nbsp; </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I have working experience and knowledge in hardware and virtualized servers for web and database application. I can perform installation, configuration, maintenance and deployment of servers on shared hosting, VPS, dedicated servers in-house or remote datacenters as well as in cloud platforms like AWS and Digital Ocean. I have knowledge and awareness on Data Privacy Compliance, ISO Standards, ITIL, PCI DSS compliance, Disaster Recovery and Business Continuity concepts and practice. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You can download <a rel=\"noreferrer noopener\" aria-label=\"my resume (opens in a new tab)\" href=\"https://drive.google.com/file/d/17RNFwbQqZUrOYS9QtOr80j9-QG2ecavA/view?usp=sharing\" target=\"_blank\">my resume</a>, visit <a href=\"https://www.facebook.com/calixto.ong\">my facebook</a> or <a href=\"https://www.linkedin.com/in/bhongong/\" target=\"_blank\" rel=\"noreferrer noopener\" aria-label=\"linkedin profile (opens in a new tab)\">linkedin profile</a> for more details. <br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Send me note:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[contact-form-7 id=\"19\" title=\"Contact form 1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','About Me','','inherit','closed','closed','','21-revision-v1','','','2019-02-09 16:51:31','2019-02-09 08:51:31','',21,'http://localhost/wptest/2019/02/09/21-revision-v1/',0,'revision','',0),
(41,1,'2019-02-09 16:53:10','0000-00-00 00:00:00','{\n    \"zerif-lite-child::background_preset\": {\n        \"value\": \"custom\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-02-09 08:53:10\"\n    }\n}','','','auto-draft','closed','closed','','26568244-c087-4d5c-8bcd-91f5a6dcdd38','','','2019-02-09 16:53:10','0000-00-00 00:00:00','',0,'http://localhost/wptest/?p=41',0,'customize_changeset','',0),
(42,1,'2019-02-09 17:28:33','2019-02-09 09:28:33','<!-- wp:paragraph -->\n<p>Hi, I am Calixto Ong II or you can call me Bhong.  Software Development, coding and coaching is my passion and I have been in the software development industry since 1994.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I have numerous experience working on a project from concept, development, testing, deployment, training and maintenance. I have years of experience working on various business applications like cooperatives, membership, school system, financial, accounting, inventory, ecommerce, travel booking, MLM, call center and health services. &nbsp;I also take into consideration, following Secure Coding Practices to ensure protection against common web vulnerability and attacks (XSS, SQL Injection, Session Hijacking and others). I am also able to pickup an existing project, perform a code review, security audit, vulnerability check, documentation, add enhancements and functionality. &nbsp; </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>I have working experience and knowledge in hardware and virtualized servers for web and database application. I can perform installation, configuration, maintenance and deployment of servers on shared hosting, VPS, dedicated servers in-house or remote datacenters as well as in cloud platforms like AWS and Digital Ocean. I have knowledge and awareness on Data Privacy Compliance, ISO Standards, ITIL, PCI DSS compliance, Disaster Recovery and Business Continuity concepts and practice. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You can download <a rel=\"noreferrer noopener\" aria-label=\"my resume (opens in a new tab)\" href=\"https://drive.google.com/file/d/17RNFwbQqZUrOYS9QtOr80j9-QG2ecavA/view?usp=sharing\" target=\"_blank\">my resume</a>, visit <a href=\"https://www.facebook.com/calixto.ong\">my facebook</a> or <a href=\"https://www.linkedin.com/in/bhongong/\" target=\"_blank\" rel=\"noreferrer noopener\" aria-label=\"linkedin profile (opens in a new tab)\">linkedin profile</a> for more details. <br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>Send me note:</h4>\n<!-- /wp:heading -->\n\n<!-- wp:shortcode -->\n[contact-form-7 id=\"19\" title=\"Contact form 1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','About Me','','inherit','closed','closed','','21-revision-v1','','','2019-02-09 17:28:33','2019-02-09 09:28:33','',21,'http://localhost/wptest/2019/02/09/21-revision-v1/',0,'revision','',0);

/*Table structure for table `testterm_relationships` */

DROP TABLE IF EXISTS `testterm_relationships`;

CREATE TABLE `testterm_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `testterm_relationships` */

insert  into `testterm_relationships`(`object_id`,`term_taxonomy_id`,`term_order`) values 
(1,1,0),
(14,2,0),
(15,2,0),
(25,2,0);

/*Table structure for table `testterm_taxonomy` */

DROP TABLE IF EXISTS `testterm_taxonomy`;

CREATE TABLE `testterm_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `testterm_taxonomy` */

insert  into `testterm_taxonomy`(`term_taxonomy_id`,`term_id`,`taxonomy`,`description`,`parent`,`count`) values 
(1,1,'category','',0,1),
(2,2,'nav_menu','',0,3);

/*Table structure for table `testtermmeta` */

DROP TABLE IF EXISTS `testtermmeta`;

CREATE TABLE `testtermmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `testtermmeta` */

/*Table structure for table `testterms` */

DROP TABLE IF EXISTS `testterms`;

CREATE TABLE `testterms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `testterms` */

insert  into `testterms`(`term_id`,`name`,`slug`,`term_group`) values 
(1,'Uncategorized','uncategorized',0),
(2,'Primary Menu','primary-menu',0);

/*Table structure for table `testusermeta` */

DROP TABLE IF EXISTS `testusermeta`;

CREATE TABLE `testusermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `testusermeta` */

insert  into `testusermeta`(`umeta_id`,`user_id`,`meta_key`,`meta_value`) values 
(1,1,'nickname','admin'),
(2,1,'first_name',''),
(3,1,'last_name',''),
(4,1,'description',''),
(5,1,'rich_editing','true'),
(6,1,'syntax_highlighting','true'),
(7,1,'comment_shortcuts','false'),
(8,1,'admin_color','fresh'),
(9,1,'use_ssl','0'),
(10,1,'show_admin_bar_front','true'),
(11,1,'locale',''),
(12,1,'testcapabilities','a:1:{s:13:\"administrator\";b:1;}'),
(13,1,'testuser_level','10'),
(14,1,'dismissed_wp_pointers','theme_editor_notice,wp496_privacy'),
(15,1,'show_welcome_panel','1'),
(16,1,'session_tokens','a:1:{s:64:\"11a51339c8c91b6424da122eba6ec0b3ba11e5405bfab8d89cec52d3476890ad\";a:4:{s:10:\"expiration\";i:1549767688;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:78:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0\";s:5:\"login\";i:1549594888;}}'),
(17,1,'testdashboard_quick_press_last_post_id','4'),
(18,1,'community-events-location','a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19,1,'testuser-settings','libraryContent=upload'),
(20,1,'testuser-settings-time','1549704143'),
(21,1,'nav_menu_recently_edited','2'),
(22,1,'managenav-menuscolumnshidden','a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(23,1,'metaboxhidden_nav-menus','a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-post_format\";}');

/*Table structure for table `testusers` */

DROP TABLE IF EXISTS `testusers`;

CREATE TABLE `testusers` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `testusers` */

insert  into `testusers`(`ID`,`user_login`,`user_pass`,`user_nicename`,`user_email`,`user_url`,`user_registered`,`user_activation_key`,`user_status`,`display_name`) values 
(1,'admin','$P$B7JZrKNg78wji84ksUWX8Nore3dFjx.','admin','bhongong@gmail.com','','2019-02-01 11:16:17','',0,'admin');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
