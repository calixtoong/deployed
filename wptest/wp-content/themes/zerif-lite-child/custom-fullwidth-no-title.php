<?php
/**
 * Template Name: Custom Full Width No Title
 *
 * @package zerif-lite-child
 */
get_header(); ?>

<div class="clear"></div>

</header> <!-- / END HOME SECTION  -->

<div id="content" class="site-content">

	<div class="container">

		<div class="content-left-wrap col-md-12">

			<h1><?php the_title(); ?></h1>

			<div id="primary" class="content-area">

				<main id="main" class="site-main">

					<?php
					while ( have_posts() ) :
						the_post();

						get_template_part( 'content', 'page-no-title' );


						endwhile;
					?>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .content-left-wrap -->

	</div><!-- .container -->

<?php get_footer(); ?>
