$(function() {

	//function for checking email format via regex
	function isEmail(chkemail) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(chkemail);
	}

	//function to display modal window displaying error message instead of regular alert
	function showAlert(alertTitle, alertMessage ) {
	  $("#error-title").html(alertTitle);
	  $("#error-message").html(alertMessage);
      $('#myModal').modal("show");
	}


	//Process Validation for Newsletter Form triggered by the button click
	$('#btn-newsletter-submit').click(function(event)
		{
			event.preventDefault();
			if ($('#newsletter-fullname').val() == "") {
				//interchangeable display for alert
				// alert('Fullname is required');
				//error shown in modal
				showAlert('Submit Newsletter', 'Fullname is required');
				$('#newsletter-fullname').focus();
				return false;
			}

			if ($('#newsletter-email').val() == "") {
				// alert('Email is required');
				showAlert('Submit Newsletter', 'Email is required');
				$('#newsletter-email').focus();
				return false;
			}

			if (!(isEmail($('#newsletter-email').val()))) {
				// alert('Email is invalid.');
				showAlert('Submit Newsletter', 'Email is invalid');
				$('#newsletter-email').focus();
				return false;
			}

			//submits form if passed with all validation
			$('#formNewsletter').submit();

		}); //END $('#btn-newsletter-submit').click(function(event)



	//Process Validation for Contact Form triggered by the button click
	$('#btn-contact-submit').click(function(event)
		{
			event.preventDefault();
			if ($('#contact-fullname').val() == "") {
				// alert('Fullname is required');
				showAlert('Contact Us', 'Fullname is required');
				$('#contact-fullname').focus();
				return false;
			}

			if ($('#contact-email').val() == "") {
				// alert('Email is required');
				showAlert('Contact Us', 'Email is required');
				$('#contact-email').focus();
				return false;
			}

			if (!(isEmail($('#contact-email').val()))) {
				// alert('Email is invalid.');
				showAlert('Contact Us', 'Email is invalid');
				$('#contact-email').focus();
				return false;
			}

			if ($('#contact-preferred-date').val() == "") {
				// alert('Preferred date is required');
				showAlert('Contact Us', 'Preferred date is required');
				$('#contact-preferred-date').focus();
				return false;
			}

			//validate if date is greater than today
			pdate = new Date($('#contact-preferred-date').val());
			ndate = new Date();
			if (pdate < ndate) {
				// alert('Preferred date is required');
				showAlert('Contact Us', 'Preferred date must be greater than today.');
				$('#contact-preferred-date').focus();
				return false;
			}

			if ($('#contact-preferred-time').val() == "") {
				// alert('Preferred time is required');
				showAlert('Contact Us', 'Preferred time is required');
				$('#contact-preferred-time').focus();
				return false;
			}

			//submits form if passed with all validation
			$('#formContact').submit();

		}); //END $('#btn-contact-submit').click(function(event)

});

/** EOF */
