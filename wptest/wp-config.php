<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp');

/** MySQL database username */
define('DB_USER', 'wp');

/** MySQL database password */
define('DB_PASSWORD', '1v!pMS)u29');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4zqat779hxgywwbfldt1eptaj33wlta5pcd28f66r8rao7aclvisxgazlh0e23gb');
define('SECURE_AUTH_KEY',  'jrvll84hatajk9aog2dmkg1d0c0lyjslmevl35yxlvtdvayty6f10hs4m7di51we');
define('LOGGED_IN_KEY',    'ukvoczyhhallmtcadvsttkjpmlhyooowh6c9ofgbowzui41ldwf8ywsghihu7cgn');
define('NONCE_KEY',        '9u6cbnb7hworn3eswukri9vujqfcylxd5jym6lxoqjo4y7lclugbdm2wrkhhhxgq');
define('AUTH_SALT',        'vsu4q3eeagsr5osqfk8kouxuuwgrerf253hcltwztmt7mmqrt7xed8ke9qsul6nk');
define('SECURE_AUTH_SALT', 'gpdv8ybn5imfa0h6cliorkxdlvrmg8stgbhmpuq7y362sjdsm5xns1jx3ss8r9wa');
define('LOGGED_IN_SALT',   '1r4ifn7fwnlmcpmg63mpxviw740u36n1rizbwxzm2iwydguco5ixqtt8ftn5red2');
define('NONCE_SALT',       'ls1uzm3m5fag49frsqiuwxa1ufewd7r2ei8ksfsd90i0rusllrhfbnb3ngd48dx1');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'test';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
